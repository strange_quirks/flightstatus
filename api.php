<?php
$result = file_get_contents('http://aviation-edge.com/api/public/flights?key=304951-e7109e-8e7f04-a56331-bc4d6c&flight[iataNumber]=' . $_REQUEST['flightNumber']);
if (!$result) {
    echo json_encode('Flight not found.');
    return;
}

$result = json_decode($result, true);
if (!$result) {
    echo json_encode('Flight not found.');
    return;
}

if (isset($result['error'])) {
    echo json_encode('Flight not found.');
    return;
}

if (isset($result[0])) {
    echo json_encode($result[0]['status']);
    return;
}

echo json_encode('Flight not found.');
return;