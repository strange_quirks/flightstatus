<!doctype html>
<html lang="en" class="no-js">
<head>
  <meta charset="UTF-8" />
  <title>Particleground demo</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="css/style.css" />
  <link rel="stylesheet" href="css/semanticui/semantic.min.css" />
  <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
  <script type='text/javascript' src='js/jquery-1.11.1.min.js'></script>
  <script type='text/javascript' src='../jquery.particleground.min.js'></script>
  <script type='text/javascript' src='js/demo.js'></script>
</head>

<body>
	<div id="particles" style="background-color: #4f6450;">
        <div id="intro">
            <div class="ui card" style="margin: auto; width: 300px;">
                <div class="content">
                    <div class="ui form">
                        <div class="field">
                            <input type="text" name="flight" placeholder="Flight number e.g BA123" autofocus id="flightInput">
                        </div>
                        <button class="ui button blue fluid" type="submit">What's the status? <i class="search icon" style="margin-left: 5px;"></i></button>
                    </div>
                </div>
            </div>
            <div class="ui card" id="statusContainer" style="margin: 20px auto; width: 300px; display: none;">
                <div class="content">
                    <h3 class="ui header">Flight status</h3>
                    <div class="flight-status" id="flightStatus"></div>
                </div>
            </div>
        </div>
	</div>
    <script>
        $("button").on("click", function() {
            $("button").toggleClass('loading');
            $.ajax({
                headers:{
                    "key":"your key",
                    "Accept":"application/json", // Depends on your api
                    "Content-type":"application/x-www-form-urlencoded" // Depends on your api
                },
                url:"http://localhost/api.php?flightNumber=" + $('#flightInput').val(),
                success: function(response){
                    var r = JSON.parse(response);
                    $("#flightStatus").html(r);

                    $("button").toggleClass('loading');

                    $("#statusContainer").show();
                }
            });
        });
    </script>
</body>
</html>
